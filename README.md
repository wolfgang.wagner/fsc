# Zoominar "Advanced Fluid Styled Content"

## vom 26.05.2021

Direkt verwendbar mit Docker+DDEV.

1. Klone dieses Repository (oder download als zip)
2. ddev config
3. ddev start
4. ddev composer install
5. ddev import-db < database.sql
6. ddev launch /typo3

Benutzername: admin\
Passwort: password

TYPO3-Zoominare und mehr auf https://wolfgangwagner.shop
